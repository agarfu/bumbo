import React, {Component} from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import RedoIcon from '@material-ui/icons/Redo';
import UndoIcon from '@material-ui/icons/Undo';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        marginTop: 10
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
});

class ControlPannel extends Component {
    constructor(props) {
        super(props)
        this.control = this.props.control
        console.log('Control pannel Control socket: ' + this.control);

        this.stop = this.stop.bind(this);
        this.move = this.move.bind(this);
        this.turn = this.turn.bind(this);
        this.rotate = this.rotate.bind(this);

        this.forward = this.forward.bind(this);
        this.backwards = this.backwards.bind(this);
        this.left = this.left.bind(this);
        this.right = this.right.bind(this);
        this.rotate_left= this.rotate_left.bind(this);
        this.rotate_right= this.rotate_right.bind(this);
    }

    stop(ev) {
        this.control.emit('stop');
    }

    move(direction, speed) {
        this.control.emit('move', {direction: direction, speed: speed});
    }

    turn(direction, speed) {
        this.control.emit('turn', {direction: direction, speed: speed});
    }

    rotate(direction, speed) {
        this.control.emit('rotate', {direction: direction, speed: speed});
    }

    forward(ev) {
        this.move('move forward', 40);
    }

    backwards(ev) {
        this.move('move backwards', 40);
    }

    left(ev) {
        this.turn('turn left', 40);
    }

    right(ev) {
        this.turn('turn right', 40);
    }

    rotate_left(ev) {
        this.rotate('rotate left', 40);
    }

    rotate_right(ev) {
        this.rotate('rotate right', 40);
    }

    render() {
        const { classes } = this.props;

        return (
        <Card className={classes.root}>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
                Control pannel
            </Typography>
            <div className={classes.controls}>
                <IconButton aria-label="forward" onMouseDown={this.forward} onMouseUp={this.stop}><ArrowUpwardIcon/></IconButton>
                <IconButton aria-label="backwards" onMouseDown={this.backwards} onMouseUp={this.stop}><ArrowDownwardIcon/></IconButton>
                <IconButton aria-label="left" onMouseDown={this.left} onMouseUp={this.stop}><ArrowBackIcon/></IconButton>
                <IconButton aria-label="right" onMouseDown={this.right} onMouseUp={this.stop}><ArrowForwardIcon/></IconButton>
                <IconButton aria-label="rotate_left" onMouseDown={this.rotate_left} onMouseUp={this.stop}><UndoIcon/></IconButton>
                <IconButton aria-label="rotate_right" onMouseDown={this.rotate_right} onMouseUp={this.stop}><RedoIcon/></IconButton>
            </div>
          </CardContent>
        </Card>
      );
    }
}

ControlPannel.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ControlPannel);
