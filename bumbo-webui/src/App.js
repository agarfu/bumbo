import React from 'react';
import './App.css';
import { Grid } from '@material-ui/core'
import Header from './Header';
import Footer from './Footer';
import CameraView from './CameraView';
import DashBoard from './DashBoard';
import ControlPannel from './ControlPannel';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import openSocket from 'socket.io-client';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  main: {
    margin: 0,
    padding: 0
  }
}));


function App() {
    const classes = useStyles();
    const control_socket = openSocket('/control');
    const feedback_socket = openSocket('/feedback');

    console.log('Control socket: ' + control_socket);
    console.log('Feedback socket: ' + feedback_socket);

    return (
        <div className={classes.root}>
            <Container component="main" className={classes.main} maxWidth={false}>
                <Grid container direction="column">
                    <Grid item>
                        <Header/>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={false} sm={1}/>
                        <Grid item xs={12} sm={7}>
                            <CameraView/>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <DashBoard feedback={feedback_socket}/>
                            <ControlPannel control={control_socket}/>
                        </Grid>
                        <Grid item xs={false} sm={1}/>
                    </Grid>
                </Grid>
            </Container>
            <Footer feedback={feedback_socket}/>
        </div>
    );
}

export default App;
