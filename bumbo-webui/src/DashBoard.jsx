import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';


class DashBoard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {left_power: 0, right_power: 0}
        this.feedback = this.props.feedback

        this.left_track_changed = this.left_track_changed.bind(this);
        this.right_track_changed = this.right_track_changed.bind(this);
        this.on_feedback = this.on_feedback.bind(this);

        this.feedback.on('tank_event', this.on_feedback);
    }

    on_feedback(msg, cb) {
        console.log('Status update: ' + msg.data);
        var data = JSON.parse(msg.data);
        if (! data.direction) {
            data.speed *= -1
        }
        var track_state = []
        track_state[data.track + '_power'] = data.speed
        this.setState(track_state);
        if (cb)
            cb();
    }

    left_track_changed(ev, val) {
        this.setState({left_power: val});
    }

    right_track_changed(ev, val) {
        this.setState({right_power: val});
    }


    render() {
        return (
        <Card style={{marginTop: '1em'}}>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
                Dashboard
            </Typography>
            <Typography id="right_track" gutterBottom>
                Right Track power: {this.state.right_power}
            </Typography>
            <Slider
                defaultValue={0}
                value={this.state.right_power}
                valueLabelDisplay="auto"
                onChange={this.right_track_changed}
                step={1}
                min={-100}
                max={100}
            />
            <Typography id="left_track" gutterBottom>
                Left Track power: {this.state.left_power}
            </Typography>
            <Slider
                defaultValue={0}
                value={this.state.left_power}
                valueLabelDisplay="auto"
                onChange={this.left_track_changed}
                step={1}
                min={-100}
                max={100}
            />
          </CardContent>
        </Card>
      );
    }
}

export default DashBoard;
