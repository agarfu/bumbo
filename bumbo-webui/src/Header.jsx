import React from 'react'
import { AppBar, Toolbar, Typography } from '@material-ui/core'
import SettingsRemoteIcon from '@material-ui/icons/SettingsRemote';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
    typographyStyles: {
        flex: 1
    }
}));

const Header = () => {
    const classes = useStyles();
    return <AppBar position="static">
                <Toolbar>
                    <Typography className={classes.typographyStyles}>
                        Bumbo, why not?
                    </Typography>
                    <SettingsRemoteIcon/>
                </Toolbar>
            </AppBar>
};

export default Header;
