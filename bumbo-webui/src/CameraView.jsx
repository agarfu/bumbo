import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';



const useStyles = makeStyles({
    media: {
        width: 770,
        height: 433,
    },
    headerStyle: {
        flex: 1,
        justifyContent: 'space-between',
    }
});

const CameraView= () => {
    const classes = useStyles();
    const [checked, setChecked] = React.useState(false);
    const cameraViews = {
        cameraOn: process.env.PUBLIC_URL + "/camera_view.png",
        cameraOff: process.env.PUBLIC_URL + "/nocamera1280x720.jpg"
    }

    const toggleChecked = () => {
        setChecked((prev) => !prev);
    };

    const imageName = checked ? 'cameraOff':'cameraOn';
    const switchLabel = checked ? 'Off' : 'On';

    return (
    <Card className={classes.root} style={{marginTop: '1em'}}>
      <CardContent>
        <CardActions>
            <Grid container className={classes.headerStyle}>
                <Grid item>
                    <Typography gutterBottom variant="h5" component="h2">
                        Viewport
                    </Typography>
                </Grid>
                <Grid item>
                    <FormGroup>
                        <FormControlLabel
                            control={<Switch checked={checked} onChange={toggleChecked} />}
                            color="primary"
                            label={switchLabel}
                        />
                    </FormGroup>
                </Grid>
            </Grid>
        </CardActions>
      </CardContent>
        <CardMedia
          className={classes.media}
          image={cameraViews[imageName]}
          title="Bumbo camera display"
        />
    </Card>
  );
};

export default CameraView;
