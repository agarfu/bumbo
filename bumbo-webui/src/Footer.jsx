import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import { Grid } from '@material-ui/core'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  main: {
    margin: 0,
    padding: 0
  },
  footer: {
    padding: theme.spacing(0.1, 1),
    marginTop: 'auto',
    backgroundColor:
    theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
  },
});

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {humidity: 'N/A', temperature: 'N/A'}
        this.feedback = this.props.feedback

        this.on_feedback = this.on_feedback.bind(this);

        this.feedback.on('tank_event', this.on_feedback);
    }

    on_feedback(msg, cb) {
        console.log('Footer state update: ' + msg.data);
        var data = JSON.parse(msg.data);
        if (typeof data['room humidity'] !== 'undefined') {
            this.setState({humidity: data['room humidity']})
        } else if (typeof data['room temp'] !== 'undefined') {
            this.setState({temperature: data['room temp']})
        }
        if (cb)
            cb();
    }


    render() {
        const { classes } = this.props;
        return (
            <footer className={classes.footer}>
                <Grid container>
                    <Typography variant="body1"><b>Temp:</b> {this.state.temperature} c | <b>Hum:</b> {this.state.humidity} %</Typography>
                </Grid>
            </footer>
      );
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Footer);
