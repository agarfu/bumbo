# Bumbo
Bumbo is my pet project, it is a rover built using a tank chasis, a raspberry pi and a bunch of sensor.

# Documentation
https://agarfu.gitlab.io/bumbo/

# Hardware
Hardware description and schematics coming soon

# Software
Software features and modules coming soon
 * Can be controlled using a PS4 bluettoth controller
 * Can be controlled using a web browser
