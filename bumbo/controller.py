from bumbo.track import BaseTrack
import logging

logger = logging.getLogger(__name__)


class Controller(object):
    RIGHT = "right"
    LEFT = "left"
    MAX_SPEED = 100

    def __init__(self, right_track, left_track):
        self.feedback_channels = []
        self.linear_speed = 0
        self.angular_speed = 0
        self.angular_direction = None
        self.net_direction = None
        self.right_track = right_track
        self.left_track = left_track

        self.stop()

    def stop(self):
        self.linear_speed = 0
        self.net_direction = None
        self.move()

    def forward(self, speed):
        self.net_direction = BaseTrack.FORWARD
        self.linear_speed = speed
        self.move()

    def backward(self, speed):
        self.net_direction = BaseTrack.BACKWARDS
        self.linear_speed = speed
        self.move()

    def left(self, speed):
        self.angular_direction = Controller.LEFT
        self.angular_speed = speed
        self.move()

    def right(self, speed):
        self.angular_direction = Controller.RIGHT
        self.angular_speed = speed
        self.move()

    def stop_turn(self):
        self.angular_speed = 0
        self.angular_direction = None
        self.move()

    def rotate_right(self, speed):
        self.rotate(Controller.RIGHT, speed)

    def rotate_left(self, speed):
        self.rotate(Controller.LEFT, speed)

    def rotate(self, direction, speed):
        if self.linear_speed != 0:
            logger.warning("Rotation allowed only when stopped")
            return
        if direction == Controller.RIGHT:
            l_direction = BaseTrack.FORWARD
            r_direction = BaseTrack.BACKWARDS
        else:
            l_direction = BaseTrack.BACKWARDS
            r_direction = BaseTrack.FORWARD
        self.right_track.move(r_direction, speed)
        self.left_track.move(l_direction, speed)

    def move(self):
        l_direction = r_direction = self.net_direction
        l_speed = r_speed = self.linear_speed

        # There is angular component
        if self.angular_speed != 0:
            if self.linear_speed + self.angular_speed > Controller.MAX_SPEED:
                slow_side = self.linear_speed - (
                    (self.linear_speed + self.angular_speed) - Controller.MAX_SPEED
                )
                fast_side = Controller.MAX_SPEED
            else:
                slow_side = self.linear_speed
                fast_side = self.linear_speed + self.angular_speed

            if self.angular_direction == Controller.RIGHT:
                l_speed = fast_side
                r_speed = slow_side
            else:
                l_speed = slow_side
                r_speed = fast_side

        if r_speed != 0 and r_direction is not None:
            self.right_track.move(r_direction, r_speed)
        else:
            self.right_track.move(None, 0)

        if l_speed != 0 and l_direction is not None:
            self.left_track.move(l_direction, l_speed)
        else:
            self.left_track.move(None, 0)
