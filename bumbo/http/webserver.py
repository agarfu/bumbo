#!/usr/bin/env python
from threading import Lock
from bumbo import running_in_raspberry
from bumbo.queuecontroller import QueueController
from bumbo.video.server import VideoServer
from bumbo.bus import BumboBus
from pkg_resources import resource_filename
from flask import (
    Flask,
    request,
    copy_current_request_context,
    Response,
    send_from_directory,
)
from flask_socketio import (
    SocketIO,
    emit,
    disconnect,
)
from flask_cors import CORS
import os
import logging
import json

logger = logging.getLogger(__name__)

templates_dir = resource_filename(__name__, "templates")
static_dir = resource_filename(__name__, os.path.join("static", "static"))

logger.info("Template path: {}".format(templates_dir))
logger.info("Static path: {}".format(static_dir))


# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None
app = Flask(__name__, template_folder=templates_dir, static_folder=static_dir)
CORS(app)
socketio = SocketIO(app, async_mode=async_mode, cors_allowed_origins="*")

command_pipe = None

thread = None
thread_lock = Lock()


video_mode = VideoServer.STATIC_MODE
if running_in_raspberry():
    video_mode = VideoServer.CAMERA_MODE

video_server = VideoServer(video_mode)


def status_thread():
    while True:
        try:
            command = command_pipe.receive(0.5)
            if command is not None:
                socketio.emit(
                    "tank_event",
                    {"data": json.dumps(command)},
                    namespace="/feedback",
                    broadcast=True,
                )
        except EOFError:
            break


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def serve(path):
    if path != "" and os.path.exists(app.static_folder + "/" + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder + "/../", "index.html")


# @app.route('/')
# def index():
#    bumbo_version = pkg_resources.get_distribution('bumbo').version
#    return render_template('index.html', async_mode=socketio.async_mode, bumbo_version=bumbo_version)


def gen():
    while True:
        frame = video_server.getJPEG()
        yield (b"--frame\r\nContent-Type: image/jpeg\r\n\r\n" + frame + b"\r\n")


@app.route("/video_feed")
def video_feed():
    if not video_server.initialized:
        video_server.initialize()
    return Response(gen(), mimetype="multipart/x-mixed-replace; boundary=frame")


@socketio.on("move", namespace="/control")
def on_move(message):
    logger.info(" -> Move {} at: {}".format(message["direction"], message["speed"]))
    d = message["direction"]
    s = message["speed"]
    if d == QueueController.FORWARD:
        command_pipe.send(QueueController.FORWARD)
    elif d == QueueController.BACKWARDS:
        command_pipe.send(QueueController.BACKWARDS)
    emit("ack", {"data": "{} - {}".format(d, s)})


@socketio.on("stop", namespace="/control")
def on_stop():
    logger.info(" -> Stop")
    command_pipe.send(QueueController.STOP_MOVE)
    command_pipe.send(QueueController.STOP_TURN)
    emit("ack", {"data": "Stopping"})


@socketio.on("turn", namespace="/control")
def on_turn(message):
    logger.info(" -> Turn {} at: {}".format(message["direction"], message["speed"]))
    d = message["direction"]
    s = message["speed"]
    command_pipe.send(QueueController.FORWARD)
    if d == QueueController.TURN_RIGHT:
        command_pipe.send(QueueController.TURN_RIGHT)
    elif d == QueueController.TURN_LEFT:
        command_pipe.send(QueueController.TURN_LEFT)
    emit("ack", {"data": "{} - {}".format(d, s)})


@socketio.on("rotate", namespace="/control")
def on_rotate(message):
    logger.info(" -> Rotate {} at: {}".format(message["direction"], message["speed"]))
    d = message["direction"]
    s = message["speed"]
    if d == QueueController.ROTATE_RIGHT:
        command_pipe.send(QueueController.ROTATE_RIGHT)
    elif d == QueueController.ROTATE_LEFT:
        command_pipe.send(QueueController.ROTATE_LEFT)
    emit("ack", {"data": "{} - {}".format(d, s)})


@socketio.on("disconnect_request", namespace="/control")
def disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit("ack", {"data": "Disconnected!"}, callback=can_disconnect)


@socketio.on("my_ping", namespace="/control")
def ping_pong():
    emit("my_pong")


@socketio.on("connect", namespace="/control")
def test_connect():
    emit("ack", {"data": "Connected to bumbo!", "count": 0})


@socketio.on("disconnect", namespace="/control")
def test_disconnect():
    logger.info("Client disconnected", request.sid)


def start_app(options, queue):
    global command_pipe
    global thread
    command_pipe = BumboBus(queue)
    with thread_lock:
        if thread is None:
            pass
            # thread = socketio.start_background_task(status_thread, bus)  # What is this bus? bumbous? I guess so, but this code is dead!
    socketio.run(
        app=app, host=options.ip, port=options.port, debug=True, use_reloader=False
    )


def main():
    from multiprocessing import Queue

    class FakeOptions:
        def __init__(self):
            self.ip = "0.0.0.0"
            self.port = 9090

    start_app(FakeOptions(), Queue())


if __name__ == "__main__":
    main()
