from threading import Thread, current_thread
import logging

logger = logging.getLogger(__name__)


class QueueController:
    ARROW_SPEED = 40
    ROTATE_LEFT = "rotate left"
    ROTATE_RIGHT = "rotate right"
    TURN_RIGHT = "turn right"
    TURN_LEFT = "turn left"
    FORWARD = "move forward"
    BACKWARDS = "move backwards"
    STOP_MOVE = "stop move"
    STOP_TURN = "stop turn"

    def __init__(self, tank_controller, bus):
        self.tank = tank_controller
        self.bus = bus
        self._command_listener = None
        self._run = False

    def listen(self):
        logger.info("Starting pipe listener")
        self._command_listener = Thread(target=self._listener)
        self._run = True
        self._command_listener.start()

    def _listener(self):
        while self._run:
            try:
                command = self.bus.receive(0.5)
                if command is not None:
                    if command == "stopping bumbo":
                        logger.info("Stopping pipe listener")
                        break
                    elif command == QueueController.FORWARD:
                        self.tank.forward(QueueController.ARROW_SPEED)
                    elif command == QueueController.BACKWARDS:
                        self.tank.backward(QueueController.ARROW_SPEED)
                    elif command == QueueController.ROTATE_LEFT:
                        self.tank.rotate_left(QueueController.ARROW_SPEED)
                    elif command == QueueController.ROTATE_RIGHT:
                        self.tank.rotate_right(QueueController.ARROW_SPEED)
                    elif command == QueueController.TURN_LEFT:
                        self.tank.left(QueueController.ARROW_SPEED)
                    elif command == QueueController.TURN_RIGHT:
                        self.tank.right(QueueController.ARROW_SPEED)
                    elif command == QueueController.STOP_MOVE:
                        self.tank.stop()
                    elif command == QueueController.STOP_TURN:
                        self.tank.stop_turn()
                    else:
                        logger.error("Unknown command: {}".format(command))
            except EOFError:
                break

    def __del__(self):
        logger.info("Stopping pipe listener")
        self._run = False
        if self._command_listener != current_thread():
            self._command_listener.join()
