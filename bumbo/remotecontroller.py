from pyPS4Controller.controller import Controller
import logging

logger = logging.getLogger(__name__)


class RemoteController(Controller):
    ARROW_SPEED = 40
    ANALOG_UP_MAX = -32767
    ANALOG_LEFT_MAX = -32767
    ANALOG_RIGHT_MAX = 32767
    ANALOG_DOWN_MAX = 32767

    def __init__(self, tank_controller, **kwargs):
        Controller.__init__(self, **kwargs)
        self.tank = tank_controller

    def on_share_release(self):
        logger.debug("event")
        raise KeyboardInterrupt("Pressed x")

    def on_up_arrow_press(self):
        logger.debug("event")
        self.tank.forward(RemoteController.ARROW_SPEED)

    def on_down_arrow_press(self):
        logger.debug("event")
        self.tank.backward(RemoteController.ARROW_SPEED)

    def on_left_arrow_press(self):
        logger.debug("event")
        self.tank.left(RemoteController.ARROW_SPEED)

    def on_right_arrow_press(self):
        logger.debug("event")
        self.tank.right(RemoteController.ARROW_SPEED)

    def arrow_release(self):
        logger.debug("event")
        self.tank.stop()

    def directional_arrow_release(self):
        logger.debug("event")
        self.tank.stop_turn()

    def on_L3_up(self, value):
        logger.debug("event: %s", value)
        self.tank.forward(self.quantize_speed(value, RemoteController.ANALOG_UP_MAX))

    def on_L3_down(self, value):
        logger.debug("event: %s", value)
        self.tank.backward(self.quantize_speed(value, RemoteController.ANALOG_DOWN_MAX))

    def on_R3_right(self, value):
        logger.debug("event: %s", value)
        self.tank.right(self.quantize_speed(value, RemoteController.ANALOG_RIGHT_MAX))

    def on_R3_left(self, value):
        logger.debug("event: %s", value)
        self.tank.left(self.quantize_speed(value, RemoteController.ANALOG_LEFT_MAX))

    def on_L2_press(self, value):
        value += RemoteController.ANALOG_RIGHT_MAX
        logger.debug("event: %s", value)
        self.tank.rotate_left(
            self.quantize_speed(value, 2 * RemoteController.ANALOG_LEFT_MAX)
        )

    def on_R2_press(self, value):
        value += RemoteController.ANALOG_RIGHT_MAX
        logger.debug("event: %s", value)
        self.tank.rotate_right(
            self.quantize_speed(value, 2 * RemoteController.ANALOG_LEFT_MAX)
        )

    def quantize_speed(self, current_value, max_value):
        value = abs(current_value * 100 / max_value)
        logger.debug("quantize speed: %s", value)

        return value

    on_up_down_arrow_release = (
        on_L3_at_rest
    ) = on_L2_release = on_R2_release = arrow_release
    on_left_right_arrow_release = on_R3_at_rest = directional_arrow_release
