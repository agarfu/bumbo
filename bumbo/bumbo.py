#!/usr/bin/python3

from bumbo.systeminfo import SystemInfo
from bumbo.remotecontroller import RemoteController
from bumbo.queuecontroller import QueueController
from bumbo.controller import Controller
from bumbo.track import Track
from bumbo.http import webserver
from bumbo.bus import BumboBus
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging
import sys
import pkg_resources
from multiprocessing import Process, Pipe
from bumbo import running_in_raspberry

if running_in_raspberry():
    import RPi.GPIO as GPIO
else:
    from .fakegpio import MockedGPIO as GPIO


logger = logging.getLogger("bumbo")
ch = logging.StreamHandler()
logger.setLevel(level=logging.INFO)
formatter = logging.Formatter(
    "[%(asctime)-15s] %(module)s.%(funcName)s %(levelname)s %(message)s"
)
ch.setFormatter(formatter)
logger.addHandler(ch)


def parse_args(args=sys.argv[1:]):
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "-v",
        "--verbose",
        required=False,
        default=False,
        action="store_true",
        dest="verbose",
        help="Debug mode",
    )

    parser.add_argument(
        "-c",
        "--config",
        default=None,
        required=False,
        dest="config",
        help="Configuration file path",
    )

    parser.add_argument(
        "-p",
        "--port",
        default=9090,
        required=False,
        dest="port",
        help="HTTP listener port",
    )

    parser.add_argument(
        "-i",
        "--ip",
        default="0.0.0.0",
        required=False,
        dest="ip",
        help="HTTP listener ip",
    )

    options = parser.parse_args(args=args)
    if options.verbose:
        logger.setLevel(level=logging.DEBUG)
    return options


class Bumbo:
    # FIXME: allow GPIO configuration and PS4 pad configuration
    FW_RIGHT_PIN = 35
    BW_RIGHT_PIN = 37
    FW_LEFT_PIN = 33
    BW_LEFT_PIN = 31
    REMOTE_CONTROLLER_DEVICE = "/dev/input/js0"

    def __init__(self, bus, options=None):
        self.core_bus = bus
        self.options = options
        self.load_config()
        self.leftTrack = Track(Bumbo.FW_LEFT_PIN, Bumbo.BW_LEFT_PIN, "Left")
        self.rightTrack = Track(Bumbo.FW_RIGHT_PIN, Bumbo.BW_RIGHT_PIN, "Right")
        self.tank_controller = Controller(self.rightTrack, self.leftTrack)
        self.remote_controller = RemoteController(
            self.tank_controller,
            interface=Bumbo.REMOTE_CONTROLLER_DEVICE,
            connecting_using_ds4drv=False,
        )
        self.queue_controller = QueueController(self.tank_controller, self.core_bus)
        self.system_info = SystemInfo(self.core_bus)

    def __del__(self):
        self.queue_controller.__del__()
        self.system_info.__del__()

    def load_config(self):
        if self.options is None:
            return
        if self.options.config is not None:
            raise NotImplementedError("Configuration parsing not implemented yet")

    def start(self):
        self.queue_controller.listen()
        self.system_info.start()
        # This method never ends
        self.remote_controller.listen(timeout=6000000)


def main(args=sys.argv[1:]):
    options = parse_args(args)
    version = pkg_resources.get_distribution("bumbo").version
    logger.info("Bumbo: v{}".format(version))

    GPIO.setmode(GPIO.BOARD)
    # Pipe returns the ends of a connection
    # tank_conn is the BumboCore connection
    # http_bumbo_bus_conn is the end of the connection that for now will be used by the web interface
    # but this should evolve to a bus with multiple connected consumers
    tank_conn, http_bumbo_bus_conn = Pipe()

    core_bus = BumboBus(tank_conn)
    bumbo = Bumbo(core_bus, options)

    bumbo_http = Process(
        target=webserver.start_app, args=(options, http_bumbo_bus_conn)
    )
    logger.info(bumbo_http)
    logger.info("Starting http server")
    try:
        bumbo_http.start()
        # Because the remote controller listener never ends
        # bumbo.start is the main loop
        bumbo.start()
    except KeyboardInterrupt:
        logger.info("Ctrl+c detected, stopping bumbo")
    finally:
        logger.info("Stopping http server")
        bumbo_http.terminate()
        bumbo_http.join()
        logger.info("GPIO Cleanup")
        GPIO.cleanup()
        del bumbo


if __name__ == "__main__":
    main()
