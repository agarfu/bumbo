from threading import Lock
import logging

logger = logging.getLogger(__name__)


class BumboBus:
    def __init__(self, pipe):
        self.receive_lock = Lock()
        self.send_lock = Lock()
        self.pipe = pipe

    def send(self, message):
        with self.send_lock:
            logger.info(" <- " + str(message))
            self.pipe.send(message)

    def subscribe(self, callback):
        """This method should be implemented to allow multiple subscribers to the events.

        It could be as simple as an array of callbacks to loop over them when a new message
        is detected in the bus. Those calls should be executed in parallel using threads
        for callback in self.registered_listeners:
            Thread(target=callback).start()
        """
        raise NotImplementedError("Configuration parsing not implemented yet")

    def receive(self, timeout):
        """The only reason why this works is because there is only one thread
        consumining this messages. To implemnt this in a more generic way the
        threads should subscribe to the events and the event bus should fan out
        the mesaages to all subscribed threads.

        The logical next step is to implement the subscribe functionality and push
        the receaving thread in this class, not in the QueueController as it stands right now.
        """
        message = None
        with self.receive_lock:
            if self.pipe.poll(timeout):
                message = self.pipe.recv()
                logger.info(str(message))
        return message
