import os
import logging

logger = logging.getLogger("bumbo")


def running_in_raspberry():
    raspi = os.uname()[4] == "armv7l"
    if raspi:
        logger.info("Detected Raspberry Pi platform")
    else:
        logger.warning("No Raspberry Pi platform detected")
    return raspi
