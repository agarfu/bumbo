from math import floor
import logging

logger = logging.getLogger(__name__)


class BaseTrack(object):
    MAX_SPEED = 100
    SPEED_STEPS = 20
    MIN_ALLOWED_SPEED = 10
    SPEED_NOISE_FILTER = 10
    FORWARD = True
    BACKWARDS = False

    def __init__(self, name="", bus=None):
        self.bus = bus
        self.name = name
        self.speed = 0
        self.is_moving = None

    def _feedback(self):
        if self.bus is not None:
            self.bus.send(
                {
                    "track": self.name.lower(),
                    "direction": self.is_moving,
                    "speed": self.speed,
                }
            )
        else:
            logger.warning(
                "Track {} is not connected to any communication bus".format(
                    self.name.lower()
                )
            )

    def move(self, direction, speed):
        speed = self.speed_control(speed)
        if speed == 0:
            self._stop()
        elif self.is_moving == direction:
            self._update_speed(speed)
        else:
            self._stop()
            self._start_moving(direction, speed)

    def speed_control(self, input_speed):
        # Limited to BaseTrack.MAX_SPEED
        input_speed = min(input_speed, BaseTrack.MAX_SPEED)

        step_ratio = BaseTrack.MAX_SPEED / BaseTrack.SPEED_STEPS
        speed = floor(input_speed / step_ratio) * step_ratio
        if speed < BaseTrack.MIN_ALLOWED_SPEED:
            return 0
        else:
            return speed

    def _update_speed(self, speed):
        if self.speed != speed:
            self.speed = speed
            self._feedback()
            logger.debug("Update {} speed: {}".format(self.name, self.speed))

    def _start_moving(self, direction, speed):
        self.is_moving = direction
        self.speed = speed
        self._feedback()
        logger.debug("Start moving {} speed: {}".format(self.name, self.speed))

    def _stop(self):
        if self.is_moving is not None:
            logger.debug("Stopping {}".format(self.name))
            self.is_moving = None
            self.speed = 0
            self._feedback()
