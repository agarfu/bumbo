from bumbo.track.basetrack import BaseTrack
from bumbo.track.directattachedtrack import DirectAttachedTrack

Track = DirectAttachedTrack

__all__ = ["BaseTrack", "Track"]
