from bumbo.track.basetrack import BaseTrack

from bumbo import running_in_raspberry

if running_in_raspberry():
    import RPi.GPIO as GPIO
else:
    from bumbo.fakegpio import MockedGPIO as GPIO
import logging

logger = logging.getLogger(__name__)


class DirectAttachedTrack(BaseTrack):
    """
    This is an implementation of track control.

    The current implementation asumes that the h-bridge that controls the track is connected to the
    system where the software is running.
    """

    def __init__(self, fw_gpio, bw_gpio, name="", bus=None):
        super().__init__(name, bus)
        GPIO.setup([fw_gpio, bw_gpio], GPIO.OUT)
        self.fw_pwm = GPIO.PWM(fw_gpio, BaseTrack.MAX_SPEED)
        self.bw_pwm = GPIO.PWM(bw_gpio, BaseTrack.MAX_SPEED)
        self.pwms = {BaseTrack.FORWARD: self.fw_pwm, BaseTrack.BACKWARDS: self.bw_pwm}

    def get_pwm(self, direction=None):
        if direction is None:
            direction = self.is_moving
        return self.pwms[direction]

    def _start_moving(self, direction, speed):
        super()._start_moving(direction, speed)
        self.get_pwm().start(self.speed)

    def _update_speed(self, speed):
        if self.speed != speed:
            self.get_pwm().ChangeDutyCycle(speed)
        super()._update_speed(speed)

    def _stop(self):
        if self.is_moving is not None:
            self.get_pwm().stop()
        super()._stop()
