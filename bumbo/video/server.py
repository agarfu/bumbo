#!/usr/bin/env python
from threading import Lock, Thread, current_thread
from bumbo import running_in_raspberry
from pkg_resources import resource_filename
import os
import logging
import cv2

if running_in_raspberry():
    import picamera  # noqa: F401

logger = logging.getLogger(__name__)


class VideoServer:
    CAMERA_MODE = "camera"
    STATIC_MODE = "picture"

    def __init__(self, mode, width=1280, height=720):
        self.mode = mode
        self.width = width
        self.height = height
        self.image_lock = Lock()
        self._capture_t = None
        self.vc = None
        self.initialized = False
        self.picture = None
        self.jpg = None
        self.run = False

    def __del__(self):
        logger.debug("Releasing video capture device")
        if self.mode == VideoServer.CAMERA_MODE:
            self.run = False
            if (
                self._capture_t is not None
                and self._capture_t != current_thread()
                and self._capture_t.isAlive()
            ):
                self._capture_t.join()
            if self.vc is not None:
                self.vc.release()

    def initialize(self):
        if self.initialized:
            return
        self.initialized = True
        self.run = False
        if self.mode == VideoServer.CAMERA_MODE:
            logger.info("Initializing picamera")
            self.vc = cv2.VideoCapture(0)
            self.vc.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
            self.vc.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)
            logger.info("Video capture device: {}".format(self.vc))
            self._capture_t = Thread(target=self._capture)
            grabbed = False
            while not grabbed:
                grabbed, self.picture = self.vc.read()
            # Start capturing thread, optimize to do it only when needed?
            # possibly is a good idea to have a different process sharing memory for this
            # to enable image processing and not being limited by the GIL
        else:
            static_dir = resource_filename("bumbo.video", "static")
            self.picture = cv2.imread(os.path.join(static_dir, "nocamera1280x720.jpg"))
            with open(
                os.path.join(static_dir, "nocamera1280x720.jpg"), "rb"
            ) as picture_file:
                self.jpg = picture_file.read()

    def getPicture(self):
        self.startCapture()
        with self.image_lock:
            copy = []
            copy[:] = self.picture
            return self.picture

    def getJPEG(self):
        if self.mode == VideoServer.CAMERA_MODE:
            return cv2.imencode(".jpg", self.getPicture())[1].tobytes()
        else:
            return self.jpg

    def startCapture(self):
        if self.mode == VideoServer.STATIC_MODE:
            return

        if self.run:
            return
        self.run = True
        self._capture_t.start()

    def _capture(self):
        logger.info("Starting capture thread")
        while self.run:
            grabbed, frame = self.vc.read()
            if not grabbed:
                continue
            with self.image_lock:
                self.picture = frame
