#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from setuptools import setup
import setuptools.command.build_py
from distutils.command.clean import clean
import shutil

import subprocess

running_in_raspberry = os.uname()[4] == 'armv7l'

setup_reqs = [
    'pytest-cov',
    'pytest',
    'flake8',
    'mock',
    'Sphinx',
    'sphinxcontrib-programoutput',
]

install_reqs = [
    'pyyaml',
    'pyPS4Controller',
    'flask>=1.1.2',
    'flask-cors',
    'flask-socketio',
    'Werkzeug>=1.0.1',
    'opencv-python',
    'psutil',
    'pigpio'
]

if running_in_raspberry:
    install_reqs.append('RPi.GPIO')
    install_reqs.append('picamera')
    # numpy 1.13.3 is the one that works in my raspbian installation
    install_reqs.append('numpy==1.13.3')
else:
    install_reqs.append('mock')
    install_reqs.append('numpy')


class BuildPyCommand(setuptools.command.build_py.build_py):
    def run(self):
        subprocess.Popen('yarn build'.split(' '), cwd=os.path.join(os.path.dirname(__file__), 'bumbo-webui')).wait()
        setuptools.command.build_py.build_py.run(self)

class CleanCommand(clean):
    def run(self):
        static_path = os.path.join(os.path.dirname(__file__), 'bumbo', 'http', 'static')
        shutil.rmtree(static_path)
        os.makedirs(static_path)
        clean.run(self)

config = {
    'cmdclass': {
        'build_py': BuildPyCommand,
        'clean': CleanCommand,
    },
    'description': 'Bumbo core',
    'author': 'Rene Martin Rodriguez',
    'url': 'https://gitlab.com/agarfu/bumbo',
    'download_url': 'https://gitlab.com/agarfu/bumbo',
    'author_email': 'agarfu@gmail.com',
    'version': '0.5.0',
    'install_requires': install_reqs,
    'scripts': [],
    'name': 'bumbo',
    'setup_requires': ['pytest-runner', 'flake8'],
    'tests_require': setup_reqs,
    'extras_require': {
        'test': setup_reqs
    },
    'packages': ['bumbo', 'bumbo.http', 'bumbo.video'],
    'entry_points': {
            'console_scripts': [
                'bumbo = bumbo.bumbo:main',
                'wumbo = bumbo.http.webserver:main',
            ],
        },
    'include_package_data': True,
}

setup(**config)


