#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.bumbo import Bumbo
from bumbo.bus import BumboBus
from multiprocessing import Pipe
import logging

logger = logging.getLogger(__name__)


class TestBumbo(unittest.TestCase):
    def setUp(self):
        self.bumbo_core, self.other_end = Pipe()
        self.bus = BumboBus(self.bumbo_core)
        self.options = MagicMock()
        self.options.config = None
        self.bumbo = Bumbo(self.bus, self.options)

    def tearDown(self):
        pass

    def test_start(self):
        self.bumbo.remote_controller = MagicMock()
        self.bumbo.queue_controller = MagicMock()
        self.bumbo.system_info = MagicMock()
        self.bumbo.start()
        self.bumbo.remote_controller.listen.assert_called_once_with(timeout=6000000)
        self.bumbo.queue_controller.listen.assert_called_once_with()
        self.bumbo.system_info.start.assert_called_once_with()
