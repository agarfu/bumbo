#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.controller import Controller
from bumbo.track import Track
import logging

logger = logging.getLogger(__name__)


class TestController(unittest.TestCase):
    def setUp(self):
        self.right_track = MagicMock()
        self.left_track = MagicMock()
        self.controller = Controller(self.right_track, self.left_track)
        self.right_track.reset_mock()
        self.left_track.reset_mock()

    def test_post_initialize(self):
        self.assertEqual(self.controller.linear_speed, 0)
        self.assertEqual(self.controller.net_direction, None)

    def test_stop(self):
        self.controller.stop()
        self.right_track.move.assert_called_once_with(None, 0)
        self.left_track.move.assert_called_once_with(None, 0)
        self.assertEqual(self.controller.linear_speed, 0)
        self.assertEqual(self.controller.net_direction, None)

    def test_forward(self):
        speed = 50
        self.controller.forward(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 50)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 50)
        self.assertEqual(self.controller.linear_speed, 50)
        self.assertEqual(self.controller.net_direction, Track.FORWARD)

    def test_backward(self):
        speed = 50
        self.controller.backward(speed)
        self.right_track.move.assert_called_once_with(Track.BACKWARDS, 50)
        self.left_track.move.assert_called_once_with(Track.BACKWARDS, 50)
        self.assertEqual(self.controller.linear_speed, 50)
        self.assertEqual(self.controller.net_direction, Track.BACKWARDS)

    def test_rotate_right(self):
        speed = 50
        self.controller.rotate_right(speed)
        self.right_track.move.assert_called_once_with(Track.BACKWARDS, 50)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 50)
        self.assertEqual(self.controller.linear_speed, 0)
        self.assertEqual(self.controller.net_direction, None)

    def test_rotate_left(self):
        speed = 50
        self.controller.rotate_left(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 50)
        self.left_track.move.assert_called_once_with(Track.BACKWARDS, 50)
        self.assertEqual(self.controller.linear_speed, 0)
        self.assertEqual(self.controller.net_direction, None)

    def test_stop_turn(self):
        self.controller.angular_speed = 50
        self.controller.angular_direction = Controller.LEFT
        self.controller.stop_turn()
        self.assertEqual(self.controller.angular_speed, 0)
        self.assertEqual(self.controller.angular_direction, None)

    def test_rotate_while_moving(self):
        self.controller.linear_speed = 1
        speed = 50
        self.controller.rotate(Controller.RIGHT, speed)
        self.assertFalse(self.right_track.move.called)
        self.assertFalse(self.left_track.move.called)

    def test_left_while_moving(self):
        speed = 50
        self.controller.linear_speed = 10
        self.controller.net_direction = Track.FORWARD
        self.controller.left(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 60)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 10)
        self.assertEqual(self.controller.linear_speed, 10)
        self.assertEqual(self.controller.net_direction, Track.FORWARD)

    def test_right_while_moving(self):
        speed = 50
        self.controller.linear_speed = 10
        self.controller.net_direction = Track.FORWARD
        self.controller.right(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 10)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 60)
        self.assertEqual(self.controller.linear_speed, 10)
        self.assertEqual(self.controller.net_direction, Track.FORWARD)

    def test_turn_top_speed(self):
        speed = 30
        self.controller.linear_speed = 90
        self.controller.net_direction = Track.FORWARD
        self.controller.right(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 70)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 100)

    def test_turn_low_speed(self):
        speed = 30
        self.controller.linear_speed = 10
        self.controller.net_direction = Track.FORWARD
        self.controller.right(speed)
        self.right_track.move.assert_called_once_with(Track.FORWARD, 10)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 40)

    def test_move_momentum(self):
        self.controller.linear_speed = 50
        self.controller.net_direction = Track.FORWARD
        self.controller.move()
        self.right_track.move.assert_called_once_with(Track.FORWARD, 50)
        self.left_track.move.assert_called_once_with(Track.FORWARD, 50)

    # If the tank is stopped requires two signals to start turning, left/right signal
    # and fw/bw signal
    def test_angular_momentum(self):
        self.controller.linear_speed = 0
        self.controller.net_direction = None
        self.controller.angular_speed = 50
        self.controller.angular_direction = Controller.LEFT
        self.controller.move()
        self.right_track.move.assert_called_once_with(None, 0)
        self.left_track.move.assert_called_once_with(None, 0)

    def test_move_momentum_while_stopped(self):
        self.controller.linear_speed = 0
        self.controller.net_direction = None
        self.controller.move()
        self.right_track.move.assert_called_once_with(None, 0)
        self.left_track.move.assert_called_once_with(None, 0)
