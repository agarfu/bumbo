#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.queuecontroller import QueueController
import logging

logger = logging.getLogger(__name__)


class TestQueueController(unittest.TestCase):
    def setUp(self):
        self.controller = MagicMock()
        self.bus = MagicMock()
        self.queuecontroller = QueueController(self.controller, self.bus)
        self.queuecontroller._run = True

    def test_start_listening(self):
        self.bus.receive.side_effect = ["stopping bumbo"]
        self.queuecontroller.listen()

    def test_forward(self):
        self.bus.receive.side_effect = [QueueController.FORWARD, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.forward.assert_called_once_with(QueueController.ARROW_SPEED)

    def test_backwards(self):
        self.bus.receive.side_effect = [QueueController.BACKWARDS, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.backward.assert_called_once_with(QueueController.ARROW_SPEED)

    def test_rotate_left(self):
        self.bus.receive.side_effect = [QueueController.ROTATE_LEFT, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.rotate_left.assert_called_once_with(QueueController.ARROW_SPEED)

    def test_rotate_right(self):
        self.bus.receive.side_effect = [QueueController.ROTATE_RIGHT, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.rotate_right.assert_called_once_with(
            QueueController.ARROW_SPEED
        )

    def test_turn_left(self):
        self.bus.receive.side_effect = [QueueController.TURN_LEFT, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.left.assert_called_once_with(QueueController.ARROW_SPEED)

    def test_turn_right(self):
        self.bus.receive.side_effect = [QueueController.TURN_RIGHT, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.right.assert_called_once_with(QueueController.ARROW_SPEED)

    def test_stop_move(self):
        self.bus.receive.side_effect = [QueueController.STOP_MOVE, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.stop.assert_called_once_with()

    def test_stop_turn(self):
        self.bus.receive.side_effect = [QueueController.STOP_TURN, "stopping bumbo"]
        self.queuecontroller._listener()
        self.controller.stop_turn.assert_called_once_with()
