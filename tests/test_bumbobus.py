#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.bus import BumboBus
import logging

logger = logging.getLogger(__name__)


class TestBumboBus(unittest.TestCase):
    def setUp(self):
        self.pipe = MagicMock()
        self.bus = BumboBus(self.pipe)

    def test_send(self):
        self.bus.send("my message")
        self.pipe.send.assert_called_once_with("my message")

    def test_receive(self):
        received = self.bus.receive(0.5)
        self.pipe.poll.assert_called_once_with(0.5)
        self.assertEqual(received, self.pipe.recv.return_value)
