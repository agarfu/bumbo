#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.track import Track
from bumbo.fakegpio import MockedGPIO
from bumbo.bus import BumboBus
import logging

logger = logging.getLogger(__name__)


class TestTrack(unittest.TestCase):
    def setUp(self):
        bumbo_core = MagicMock()
        bus = BumboBus(bumbo_core)

        self.fw_pwm = MagicMock()
        self.bw_pwm = MagicMock()
        MockedGPIO.PWM.side_effect = [self.fw_pwm, self.bw_pwm]
        self.track = Track(1, 2, "test track", bus)

    def test_stop_while_moving_forward(self):
        self.track.is_moving = Track.FORWARD
        self.track._stop()
        self.fw_pwm.stop.assert_called_once_with()

    def test_stop_while_moving_backwards(self):
        self.track.is_moving = Track.BACKWARDS
        self.track._stop()
        self.bw_pwm.stop.assert_called_once_with()

    def test_stop_while_not_moving(self):
        self.track.is_moving = None
        self.track._stop()
        self.assertEqual(self.fw_pwm.stop.call_count, 0)
        self.assertEqual(self.bw_pwm.stop.call_count, 0)

    def test_move_fw_while_stopped(self):
        self.track.move(Track.FORWARD, 50)
        self.fw_pwm.start.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.FORWARD)

    def test_move_bw_while_stopped(self):
        self.track.move(Track.BACKWARDS, 50)
        self.bw_pwm.start.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.BACKWARDS)

    def test_move_fw_while_moving_fw(self):
        self.track.is_moving = Track.FORWARD
        self.track.move(Track.FORWARD, 50)
        self.fw_pwm.ChangeDutyCycle.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.FORWARD)

    def test_move_bw_while_moving_bw(self):
        self.track.is_moving = Track.BACKWARDS
        self.track.move(Track.BACKWARDS, 50)
        self.bw_pwm.ChangeDutyCycle.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.BACKWARDS)

    def test_move_fw_while_moving_bw(self):
        self.track.is_moving = Track.BACKWARDS
        self.track.move(Track.FORWARD, 50)
        self.bw_pwm.stop.assert_called_once_with()
        self.fw_pwm.start.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.FORWARD)

    def test_move_bw_while_moving_fw(self):
        self.track.is_moving = Track.FORWARD
        self.track.move(Track.BACKWARDS, 50)
        self.fw_pwm.stop.assert_called_once_with()
        self.bw_pwm.start.assert_called_once_with(50)
        self.assertEqual(self.track.is_moving, Track.BACKWARDS)

    def test_speed_control_above_max(self):
        self.assertEqual(
            self.track.speed_control(Track.MAX_SPEED + 10), Track.MAX_SPEED
        )

    def test_speed_control_below_min(self):
        self.assertEqual(self.track.speed_control(Track.MIN_ALLOWED_SPEED - 1), 0)

    def test_speed_control_steps(self):
        self.assertEqual(self.track.speed_control(1), 0)
        self.assertEqual(self.track.speed_control(5), 0)
        self.assertEqual(self.track.speed_control(Track.MIN_ALLOWED_SPEED), 10)
        self.assertEqual(self.track.speed_control(Track.MIN_ALLOWED_SPEED + 1), 10)
        self.assertEqual(self.track.speed_control(Track.MIN_ALLOWED_SPEED + 4), 10)
        self.assertEqual(self.track.speed_control(Track.MIN_ALLOWED_SPEED + 5), 15)
        self.assertEqual(self.track.speed_control(Track.MAX_SPEED - 1), 95)
        self.assertEqual(self.track.speed_control(Track.MAX_SPEED), 100)

    def test_move_below_the_min(self):
        self.track.is_moving = Track.BACKWARDS
        self.track.move(Track.BACKWARDS, Track.MIN_ALLOWED_SPEED - 1)
        self.bw_pwm.stop.assert_called_once_with()
        self.assertEqual(self.track.is_moving, None)
