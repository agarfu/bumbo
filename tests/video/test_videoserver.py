#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import patch
from pkg_resources import resource_filename
from bumbo.video.server import VideoServer
import cv2
import os
import logging

logger = logging.getLogger(__name__)


class TestVideoServerStaticMode(unittest.TestCase):
    def setUp(self):
        self.vs = VideoServer(VideoServer.STATIC_MODE)

    def test_initialize(self):
        self.vs.initialize()
        self.vs.initialize()
        self.assertIsNotNone(self.vs.jpg)

    def test_get_jpg(self):
        self.vs.initialize()
        jpg = self.vs.getJPEG()
        self.assertIsNotNone(jpg)

    def test_start_capture(self):
        self.vs.startCapture()
        self.assertFalse(self.vs.run)


class TestVideoServerCameraMode(unittest.TestCase):
    def setUp(self):
        self.vs = VideoServer(VideoServer.CAMERA_MODE)

        static_dir = resource_filename("bumbo.video", "static")
        self.picture = cv2.imread(os.path.join(static_dir, "nocamera1280x720.jpg"))

        with patch("bumbo.video.server.cv2") as cv2_mock:
            self.cv2_mock = cv2_mock
            self.cv2_mock.VideoCapture.return_value.read.return_value = [
                True,
                self.picture,
            ]
            self.vs.initialize()

    def tearDown(self):
        self.vs.run = False

    def test_initialize(self):
        self.assertIsNotNone(self.vs.vc)
        self.assertIsNone(self.vs.jpg)

    def test_get_jpg(self):
        jpg = self.vs.getJPEG()
        self.assertIsNotNone(jpg)
