#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from mock import MagicMock
from bumbo.remotecontroller import RemoteController
import logging

logger = logging.getLogger(__name__)


class TestRemoteController(unittest.TestCase):
    def setUp(self):
        self.tank_controller = MagicMock()
        self.ps4_pad = RemoteController(
            self.tank_controller, interface="/dev/js/testjs"
        )

    def tearDown(self):
        pass

    def test_on_share_release(self):
        with self.assertRaises(KeyboardInterrupt):
            self.ps4_pad.on_share_release()

    def test_on_up_arrow_press(self):
        self.ps4_pad.on_up_arrow_press()
        self.tank_controller.forward.assert_called_once_with(
            RemoteController.ARROW_SPEED
        )

    def test_on_down_arrow_press(self):
        self.ps4_pad.on_down_arrow_press()
        self.tank_controller.backward.assert_called_once_with(
            RemoteController.ARROW_SPEED
        )

    def test_on_left_arrow_press(self):
        self.ps4_pad.on_left_arrow_press()
        self.tank_controller.left.assert_called_once_with(RemoteController.ARROW_SPEED)

    def test_on_right_arrow_press(self):
        self.ps4_pad.on_right_arrow_press()
        self.tank_controller.right.assert_called_once_with(RemoteController.ARROW_SPEED)

    def test_arrow_release(self):
        self.ps4_pad.arrow_release()
        self.ps4_pad.on_up_down_arrow_release()
        self.ps4_pad.on_L3_at_rest()
        self.ps4_pad.on_L2_release()
        self.ps4_pad.on_R2_release()
        self.assertEqual(self.tank_controller.stop.call_count, 5)

    def test_directional_arrow_release(self):
        self.ps4_pad.directional_arrow_release()
        self.ps4_pad.on_left_right_arrow_release()
        self.ps4_pad.on_R3_at_rest()
        self.assertEqual(self.tank_controller.stop_turn.call_count, 3)

    def test_on_L3_up(self):
        self.ps4_pad.on_L3_up(RemoteController.ANALOG_UP_MAX)
        self.tank_controller.forward.assert_called_once_with(100)

    def test_on_L3_down(self):
        self.ps4_pad.on_L3_down(RemoteController.ANALOG_DOWN_MAX)
        self.tank_controller.backward.assert_called_once_with(100)

    def test_on_R3_right(self):
        self.ps4_pad.on_R3_right(RemoteController.ANALOG_RIGHT_MAX)
        self.tank_controller.right.assert_called_once_with(100)

    def test_on_R3_left(self):
        self.ps4_pad.on_R3_left(RemoteController.ANALOG_LEFT_MAX)
        self.tank_controller.left.assert_called_once_with(100)

    def test_on_L2_press(self):
        self.ps4_pad.on_L2_press(RemoteController.ANALOG_LEFT_MAX)
        self.tank_controller.rotate_left.assert_called_once_with(0)

    def test_on_R2_press(self):
        self.ps4_pad.on_R2_press(RemoteController.ANALOG_LEFT_MAX)
        self.tank_controller.rotate_right.assert_called_once_with(0)
