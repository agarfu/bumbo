===============================================
Bumbo: Having fun with random things
===============================================
The goal Bumbo is learn new skills in my spare time

https://gitlab.com/agarfu/bumbo

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Architecture
------------
This is a non sense architecture that has not being thought through enough. It is really messed up so
I needed to describe it in this document in order to be able to fix it.

It must evolve to something much more decoupled sharing status and commands via a full duplex bus.

I'd like to keep the deployment as simple as possible, so for the time being I'll try to not use any tcp service
as communication bus, but instead use python multiprocess structures to achive that goal.

My next step will be to move the VideoServer from the HTTP backend process to Bumbo Core and use the BumboBus to share the
images.

The end goal is to be able to feed som IA with images and sensor data in order to achieve some degree of autonomous handling
in order to achive that there are many changes to apply to the architecture. The PyCamaera can't be managed by the web
interface, that is for sure.

.. graphviz::

   digraph Bumbo_architecture {
       subgraph cluster_HW_platform {
        label="Bumbo HW platform";

        subgraph cluster_Bumbo {
           label="Bumbo";
           "Left track";
           "Right track";
           Controller;
           SystemInfo;
           RemoteController;
           QueueController;
        }
        
        BumboBus;

        subgraph cluster_PS4_controller {
           label="PS4 controller";
           "PS4Controller";
        }


       }

      subgraph cluster_Web_site {
        label="Bumbo web";

        subgraph cluster_backend {
           label="HTTP Backend";
           Flask;
           VideoServer;
           Picamera;

        }

        subgraph cluster_Web_frontend {
           label="HTTP frontend";
           React;
           "Socket.io";
        }
      }
      subgraph cluster_IA {
        label="IA";
        "Autonomous mode TBD";
      }

      "Socket.io" -> React [label="Revice status update"];
      React -> "Socket.io" [label="Sends control commands"];
      "Socket.io" -> Flask [label="Web socket full duplex communication channel", arrowhead=none];


      Flask -> BumboBus [arrowhead=none];
      BumboBus -> QueueController [label="Receive commands"];
      QueueController -> Controller;
      RemoteController -> Controller;
      PS4Controller -> RemoteController [label="Bluetooth"];
      Controller -> "Left track"
      Controller -> "Right track"
      "Left track" -> BumboBus [label="Sends status update"];
      "Right track" -> BumboBus [label="Sends status update"];
      SystemInfo -> BumboBus [label="HW status cpu usage, cpu temp, etc."];
      Flask -> VideoServer;
      VideoServer -> Picamera;
   }

Hardware
--------

Develop
-------
Spoiler alert, there are two different software projects in thins repository. I'll fix that in the future
by splitting the react web ui in a different repository, but for the time being, both nodejs and python are
required to build and test the project.

That is the reason why I'm providing instructions to build a Docker image with the only pourpose of serving
as development environment.


Building the dev environment docker image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A Dockerfile.dev is provided to simplify creating the development environment::

 docker build -t bumbo-dev . -f Dockerfile.dev

Build the project
~~~~~~~~~~~~~~~~~

This step is important because it will run also ``yarn build`` in order to generate the react web ui.::

 python setup.py build
 python setup.py build_sphinx


Install the project
~~~~~~~~~~~~~~~~~~~

The first command is only required if you want to have the web ui available.::

 python setup.py build
 pip install .


Testing
~~~~~~~

Once you have your development docker image, you can use it for development. The trick to cache the node_modules
directory will save you a lot of time if you're running this from a mac. I assume that it won't be necesary
running the dev environment from linux.

By default bumbo listens http in 9090/tcp that is part of the Docker image configuration.::

 localhost$ docker run --rm -it \
     -v $(pwd):$(pwd) \
     -w $(pwd)  \
     -v $(pwd)/bumbo-webui/node_modules:$(pwd)/bumbo-webui/node_modules:cached \
     bumbo-dev

Install the project and test dependencies in developer mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

 container# pip install -e .[test]


Running the tests
~~~~~~~~~~~~~~~~~
::

 container# pytest
 =================================== test session starts ====================================
 platform linux -- Python 3.7.4, pytest-5.4.2, py-1.8.1, pluggy-0.13.1
 rootdir: /Users/rrodriquez/git/agarfu/bumbo, inifile: setup.cfg
 plugins: cov-2.8.1
 collected 1 item

 tests/test_controller.py .                                                           [100%]

 ----------- coverage: platform linux, python 3.7.4-final-0 -----------
 Name                        Stmts   Miss Branch BrPart  Cover   Missing
 -----------------------------------------------------------------------
 bumbo/__init__.py               0      0      0      0   100%
 bumbo/bumbo.py                 25     25      0      0     0%   3-31
 bumbo/caterpillar.py           55     40     18      0    21%   15-20, 23-37, 40-48, 51-58, 61-68
 bumbo/controller.py            49     39     18      0    15%   6-10, 13-16, 19, 22, 25, 28, 31-37, 40-60
 bumbo/fakegpio.py               2      0      0      0   100%
 bumbo/remotecontroller.py      39     39      0      0     0%   1-54
 -----------------------------------------------------------------------
 TOTAL                         170    143     36      0    13%
 Coverage HTML written to dir coverage_html_report
 Coverage XML written to file coverage.xml

 FAIL Required test coverage of 90.0% not reached. Total coverage: 13.11%

 ==================================== 1 passed in 1.61s =====================================

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
